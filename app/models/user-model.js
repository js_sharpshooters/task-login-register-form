//user model
 (function(GLOBAL) {
        /***
         * userModel - add user in DB, check user in DB
         * @param String username
         * @param String password
         * returns function addUser();
         * returns function checkUser();
         **/
        var userModel = (function() { // (options)
            return {
                addUser: function(username,password) {
                    API.user.check(username, password, function (result) {
                        if(result[0]){
                        console.log("There is already a user with this data");
                        document.getElementById('errors').innerHTML = "There is already a user with this data";
                        }else{
                            API.user.add(username, password);
                        }
                    });
                },
                checkUser: function(username,password){
                    API.user.check(username, password, function (result2) {
                        if(result2[0]){
                            console.log("Entry");
                            document.getElementById('errors').innerHTML = "Entry";
                            return result2[0];
                        }else{
                            console.log("No entry");
                            document.getElementById('errors').innerHTML = "No entry";
                        }
                    });
                }
            };
        })();

        GLOBAL.userModel = userModel;
})(window);
