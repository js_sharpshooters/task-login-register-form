//login view

(function(GLOBAL, DOC) {

    /***
     * loggedUser - check user in DB
     * @param event
     * returns function GLOBAL.loggedUser
    **/
    function loggedUser(event) {

        var errors = [];

        event.preventDefault();

        var username = DOC.getElementById('username').value,
            password = DOC.getElementById('password').value;

            errors=userModel.checkUser(username, password);
            DOC.getElementById('errors').innerHTML = "Incorrect username or pass"+ errors;

            DOC.getElementById('username').value = '';
            DOC.getElementById('password').value = '';
            // DOC.getElementById('errors').innerHTML='';
            // GLOBAL.Router.goTo('/home');
    }

    GLOBAL.loggedUser = loggedUser;

})(window);
