//register view

(function(GLOBAL, DOC) {

    /***
     * registerUser - register user in DB
     * @param event
     * returns function GLOBAL.registerUser
    **/
    function registerUser (event) {

        var errors = {};

        event.preventDefault();

        checkLength('reg-user', 'registerform', errors, 5, 'Username');
        checkCharacters('reg-user', 'registerform', errors, 'Username');
        checkLength('reg-pass', 'registerform', errors, 3, 'Password');
        checkRepeat('reg-pass', 'confirm_password', 'registerform', errors, 'Passwords');


        if (Object.keys(errors).length > 0) {
            var result = '';

            for(var error in errors) {
                result += '<p>' + errors[error] + '</p>';
            }

            DOC.getElementById('errors').innerHTML = result;
        }
        else {
            var username = DOC.getElementById('reg-user').value,
                password = DOC.getElementById('reg-pass').value;

            userModel.addUser(username, password);

            DOC.getElementById('reg-user').value = '';
            DOC.getElementById('reg-pass').value = '';
            DOC.getElementById('confirm_password').value = '';
            DOC.getElementById('errors').innerHTML='';

            GLOBAL.Router.goTo('/home');
        }
    }

    GLOBAL.registerUser = registerUser;
})(window, document);
