/***
 * Base view class
 */

window.Env = window.Env || {};

(function(GLOBAL, DOC){
    var BaseView = (function() {
        var _options = {
            data: {},
            template: null,
            id: null
        };

        /***
         * create - Sets the properties of the private _options variable
         *          in the following form:
         *          {id: ..., template: ..., data: ...}
         * @param {Object} options
         * returns {Object} api
         **/
        function create(options) {
            var viewEvent;

            if (options.id) {
                _options.id = options.id;
                _options.placeholder = document.getElementById(_options.id);
            }

            if (options.template) {
                _options.template = options.template;
            }

            if (options.data) {
                _options.data = options.data;
            }

            if (_options.id && _options.template) {
                //create a custom event to fire when the ajax call is completed
                viewEvent = new Event('showView');
                _options.placeholder.addEventListener('showView', render);

                //check for cached template
                if (localStorage[_options.template]) {
                    _options.loadedTemplate = localStorage[_options.template];
                    //fire render()
                    _options.placeholder.dispatchEvent(viewEvent);
                } else {
                    loadHTML(_options.template, function(templ) {
                        localStorage[_options.template] = templ;
                        _options.loadedTemplate = templ;
                        //fire render() after call completion
                        _options.placeholder.dispatchEvent(viewEvent);
                    });
                }
            } else {
                console.error('Nothing to render');
            }

            return api;
        }

        /***
         * render - renders the html for the user-set view
         * returns void
         **/
        function render() {
            setVisibility(_options.id);
            _options.placeholder.innerHTML = _.template(_options.loadedTemplate, _options.data);
        }

        /***
         * bindEvents - binds the user-given events
         *              in the following forms:
         *              'selector event' : 'handler'
         *              'selector event' : function(e) {...}
         * @param {Object} events
         * returns {Object} api
         **/
        function bindEvents(events) {
            var method,
                property,
                temp,
                compiledEvents = '';

            for (property in events) {
                //property = 'selector event' => temp[0] = selector, temp[1] = event
                temp = property.split(' ');
                elements = document.querySelectorAll(temp[0]);
                Array.prototype.forEach.call(elements, function(el) {
                    //events[property] = event handler
                    el.addEventListener(temp[1], events[property]);
                });
            }

            return api;
        }

        /***
         * unbindEvents - unbinds the user-given events
         *                in the following forms:
         *                'selector event' : 'handler'
         *                'selector event' : function(e) {...}
         * @param {Object} events
         * returns {Object} api
         **/
        function unbindEvents(events) {
            var method,
                property,
                temp,
                compiledEvents = '';

            for (property in events) {
                //property = 'selector event' => temp[0] = selector, temp[1] = event
                temp = property.split(' ');
                elements = document.querySelectorAll(temp[0]);
                Array.prototype.forEach.call(elements, function(el) {
                    //events[property] = event handler
                    el.removeEventListener(temp[1], events[property]);
                });
            }

            return api;
        }

        /**
         * Sets css ".active" class for the navigation and the content sections.
         *
         * @param {String} id The id of the content.
         * @author Aleksanda Rusakov
         */
        function setVisibility(id) {

            var navLink = id.replace('placeholder-', '');

            // Set the new selected navigation button
            try {
                DOC.querySelector('.navigation > .active-link').classList.remove('active-link');
                DOC.querySelector('a[href="#/'+ navLink + '"]').classList.add('active-link');
            }
            catch(e) {
            }

            // Set the new visible conteiner
            DOC.querySelector('.wrapper .active').classList.remove('active');
            DOC.getElementById(id).classList.add('active');
        }


        var api = {
            create: create,
            bindEvents: bindEvents,
            unbindEvents: unbindEvents
        };

        return api;
    });

    GLOBAL.BaseView = BaseView;
})(window, document);

