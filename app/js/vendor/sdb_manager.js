/**
 * Data base Manager Class
 * Author: Samuil Gospodinov
 */
window.API = window.API || {};

(function(API){
    /***
     * DB Instance Private object
     */
    var DB = {};

    /***
     * Users Table definition
     */
    var usersTable = {
        tableName: 'users',
        fields: [
            {
                fieldName: 'id',
                fieldType: 'UNIQUE'
            },
            {
                fieldName: 'username',
                fieldType: 'TEXT'
            },
            {
                fieldName: 'password',
                fieldType: 'TEXT'
            }
        ]
    };

    /***
     * Documents Table definition
     */
    var documentsTable = {
        tableName: 'documents',
        fields: [
            {
                fieldName: 'id',
                fieldType: 'UNIQUE'
            },
            {
                fieldName: 'title',
                fieldType: 'TEXT'
            },
            {
                fieldName: 'text',
                fieldType: 'TEXT'
            },
            {
                fieldName: 'date',
                fieldType: 'DATE'
            },
            {
                fieldName: 'userid',
                fieldType: 'TEXT'
            },
            {
                fieldName: 'url',
                fieldType: 'TEXT'
            },
            {
                fieldName: 'note',
                fieldType: 'TEXT'
            }
        ]
    };

    API.firestart = function() {

        DB.init()
            .table.create(usersTable)
            .table.create(documentsTable);
    };


    /**
     * User handling
     */
    API.user = {

        /**
         * Add User
         * @param [String] user
         * @param [String] pass
         * returns API
         */
        add: function(user, pass) {

            var userObject = {
                table: 'users',
                fields: [
                            {name: 'id', value: (new Date()).getTime()},
                            {name: 'username', value: user},
                            {name: 'password', value: pass}
                        ]
            };

            DB.record.add(userObject);

            return API;
        },

        /* by ANi
        * Remove By Username
        * @param [String] usernamee
        * returns API
        */

        removeByUsername:function(usernamee){
            var userObject = {
                table: 'users',
                name: 'username',
                value: usernamee
            };
            DB.record.remove(userObject);
            return API;
        },

        /* by Ani
        * Remove By Id
        * @param [Int] id
        * returns API
        */
        removeById:function(id){
            var userObject = {
                table: 'users',
                name: 'id',
                value: id
            };
            DB.record.remove(userObject);
            return API;
        },

        /**
         * Check User - succes equals login
         * @param [String] user
         * @param [String] pass
         * @param callBack - user callback function to get the result data - Array [obeject: id, username] || []
         */
        check: function(user, pass, callBack) {
            var self = this;

            var userData = {
                table: 'users',
                selectFieleds: [{name: 'id, username'}],
                limitFieleds: [
                    {
                        name: 'username',
                        value: user
                    },
                    {
                        name: 'password',
                        value: pass
                    }
                ]
            };

            DB.record.select(userData, function(result){
               callBack && callBack(result);
            });
        }
    };

    /**
     * Documents handler
     */
    API.documents = {
        /**
         * Add a Document to the data bese
         * @param [Object] documentObject
         * @param [Function] callback
         * returns API
         */
        add: function(documentObject, callback) {

            //Example
            //API.documents.add({title: 'title of document', text: 'some text', userId: 123456, url: null, note: null});

            //If we have the mandotary fields for the document definition
            if(documentObject.title && documentObject.userid) {
                //documentObject
                var currentDate = new Date();
                //Reconfigure object for DB insertion format
                documentObject = {
                    table: 'documents',
                    fields: [
                        {name: 'id', value: currentDate.getTime()},
                        {name: 'title', value: documentObject.title},
                        {name: 'text', value: documentObject.text || null},
                        {name: 'date', value: currentDate.toLocaleDateString()},
                        {name: 'userid', value: documentObject.userId},
                        {name: 'url', value: documentObject.url || null},
                        {name: 'note', value: documentObject.note || null}
                        ]
                };

                DB.record.add(documentObject);
                //TO DO: refactor this
                callback && callback(documentObject.fields[0].value, documentObject.fields[3].value);

            }else {
                console.log(['Error: Document definition incomplete, plese check you data', documentObject]);
            }

            return API;
        },

        /**
         * Get all documents for a user
         * @param [String] userId
         * returns API
         */
        getAll: function(userId, callBack) {
            var self = this;

            var userData = {
                table: 'documents',
                selectFieleds: [{name: '*'}],
                limitFieleds: [
                    {
                        name: 'userid',
                        value: userId
                    }
                ]
            };

            DB.record.select(userData, function(result){
                callBack && callBack.call(self, result);
            });
        }

    };

    /**
     * DB - SQL builder
     */
    DB.instance = null;

    /**
     * Initialize and open connection to DB if available
     * returns DB if successfullty initiated DB
     */
    DB.init = function() {
        alertify.set({ delay: 4000 });
        alertify.log("Initialising database");
        try {
            if (window.openDatabase) {
                DB.instance = openDatabase("documentManager", "1.0", "HTML 5 Database API example", 200000);
                alertify.success("DB initiated");

                return DB;
            } else {
                alertify.error("DB initialization error");
            }
        } catch (e) {
            alertify.error("error occurred during DB init, Web Database supported?");
        }
    };


    /**
     * Database Tables handler
     */
    DB.table = {
        /**
         * Create table in current database
         * @param [Json Object] options
         *                      options.tableName - table name
         *                      options.fields - fields array
         *                               fields.fieldName
         *                               fields.fieldType
         * returns DB
         */
        create: function(options) {
            var query = "CREATE TABLE IF NOT EXISTS " + options.tableName + " (";

            for(var i = 0; options.fields.length > i; i++) {
                query += options.fields[i].fieldName + " " + options.fields[i].fieldType;

                if(i < options.fields.length-1) {
                    query += ', ';
                }
            }

            query += ')';

            try {
                DB.instance.transaction(function(tx) {

                    tx.executeSql(
                        query,[],
                        DB_Transaction_.onReadyTransaction,
                        DB_Transaction_.onError
                    );
                });
            }   catch (e) {
                alertify.error("Query execution error");
                console.log('Query execution error? -> ', e);
            }

            return DB;
        }
    };

    DB.record = {

        //Example code
        // DB.record.add({table: 'users', fields: [{name: 'id', value: 1}, {name: 'username', value: 'gosho'}, {name: 'password', value: '123456'}]});

        /**
         * Add record to table function
         * @param [Object] options
         *                  options.table
         *        [Array]   options.fields
         * returns DB
         */
        add: function(options) {

            var query = 'INSERT INTO ' + options.table + ' (';
            var optionString = '';

            for(var i = 0; options.fields.length > i; i++) {
                query += options.fields[i].name;
                optionString += '\'' + options.fields[i].value + '\'';

                if(options.fields.length-1 > i) {
                    query += ', ';
                    optionString += ', ';
                }
            }

            query += ') VALUES (' + optionString + ')';

            console.log(query);

            DB.instance.transaction(function(tx) {

                tx.executeSql(query,[],
                    DB_Transaction_.onReadyTransaction,
                    DB_Transaction_.onError
                );

            });

            return DB;
        },

        /**
         * Update record to table function
         * @param [Object] options
         *                  options.table
         *        [Array]   options.fields
         * returns DB
         */
        update: function(options,id) {

            var query = 'UPDATE ' + options.table + ' SET ';
            for(var i = 0; options.selectFieleds.length > i; i++) {
                query += options.selectFieleds[i].name + '=\'' + options.selectFieleds[i].value + '\'';

                if(options.selectFieleds.length-1 > i) {
                    query += ', ';
                }
            }
            query += ' WHERE id = '+id;
            console.log(query);

            DB.instance.transaction(function(tx) {

                tx.executeSql(query,[],
                    DB_Transaction_.onReadyTransaction,
                    DB_Transaction_.onError
                );

            });

            return DB;
        },



        //DB.record.remove({table: 'users', {name: 'username', value: 'gosho'}});

        /**
         * Remove record from table
         * @param [Object] options
         *                  options.table
         *        [Obejct]  options.field
         *                          field.name
         *                          field.value
         * returns DB
         */
        // remove: function(options) {
        //     var query = 'DELETE from ' + options.table + ' WHERE ' + options.field.name + ' = \'' + options.field.value + '\'';
        //     console.log(query);
        //     DB.instance.transaction(function(tx) {
        //         tx.executeSql(query,
        //             [],
        //             DB_Transaction_.onReadyTransaction,
        //             DB_Transaction_.onError);
        //     });

        //     return DB;
        // },
        // function remove didn't work right, so I fixed it (by ANi)
        remove: function(options) {
            var query = 'DELETE from ' + options.table + ' WHERE ' + options.name + ' = \'' + options.value + '\'';
            console.log(query);
            DB.instance.transaction(function(tx) {
                tx.executeSql(query,
                    [],
                    DB_Transaction_.onReadyTransaction,
                    DB_Transaction_.onError);
            });

            return DB;
        },

        /**
         * Select record(s) from table
         * @param [Object] options
         *                  options.table
         *
         *        [Array of Objects]   options.selectFieleds
         *        [Object property]    selectFieleds[i].name
         *
         *        [Array of Obejct]  options.limitFieleds
         *                              limitFieleds[i].name
         *                              limitFieleds[i].value
         *
         * @param [Function] callback - resultset will be passed to this function
         * returns DB
         */
        select: function(options, callBack) {

            // {
            //     table: 'users',
            //     selectFieleds: [{name: '*'}],
            //     limitFieleds: [{name: 'username', value: 'samuil'}]
            // }
            var resultSet = [];
            var query = 'SELECT ';

            for(var m=0; m<options.selectFieleds.length; m++) {

                query += options.selectFieleds[m].name;

                if (m > 0 && m < options.selectFieleds.length-1) {
                    query += ',';
                }
            }

            query += ' from ' + options.table;

            if(options.limitFieleds.length) {

                 query += ' WHERE ';

                for(var n=0; n<options.limitFieleds.length; n++) {
                    query += options.limitFieleds[n].name + ' = \'' + options.limitFieleds[n].value + '\' ';

                    if (n < options.limitFieleds.length-1) {
                        query += 'AND ';
                    }
                }
            }

            DB.instance.transaction(function(tx) {
                tx.executeSql(query,
                    [],
                    function (tx, result) {



                        for(var i=0; i<result.rows.length; i++) {
                            //console.log(result.rows.item(i));
                            resultSet.push(result.rows.item(i));
                         }

                         callBack && callBack(resultSet);
                    },
                    DB_Transaction_.onError);
            });

            return DB;
        }
    };

    /**
     * @private
     * DB transactions callback functions
     */
    var DB_Transaction_ = {

        onReadyTransaction: function transactionReady (tx, result) {
            alertify.success('Transaction completed');
            console.dir([result, tx]);
        },

        onSuccessExecuteSql: function transactionCompleted (tx, result) {
            alertify.success('Execute SQL completed');
            console.dir([result, tx]);
        },

        onError: function transactionError (transaction, error) {
            alertify.error('SQLError: ' + error.message);
            console.log(['SQLError: ', error.message]);
        }

    };

    API.firestart();

})(API);
