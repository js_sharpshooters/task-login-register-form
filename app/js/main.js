/**
 * Master application
 */

// window.Env = window.Env || {};

(function(GLOBAL) {

    GLOBAL.Router
    .config({
        root: '/login',
        has404: true
    })
    .add('/404', function() {
        var error404View = new BaseView();
        error404View.create({id: 'placeholder-404', template: '404'});
    })
    .add('/register', function() {
        var registerView = new BaseView();
        registerView
        .create({id: 'placeholder-register', template: 'register'})
        .unbindEvents({'#login submit': GLOBAL.loggedUser})
        .bindEvents({'#register submit': GLOBAL.registerUser});
    })
    .add('/login', function() {
        var loginView = new BaseView();
        loginView
        .create({id: 'placeholder-login', template: 'login'})
        .unbindEvents({'#register submit': GLOBAL.registerUser})
        .bindEvents({'#login submit': GLOBAL.loggedUser});
    })
    .add('/home', function() {
        var homeView = new BaseView();
        homeView.create({id: 'placeholder-home', template: 'home'});
    })
    .start();

})(window);
