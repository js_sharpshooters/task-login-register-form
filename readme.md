# Training task - create a login and a register form
## Please use the predefined API


## Application structure - Folders:

* app - holds the following sub folders
    * **core** - holds your core application files
    * **js** - holds the vendor JS libraries
    * **models** - OPTIONAL - holds the user model for your application
    * **templates** - OPTIONAL - holds the HTML templates your views
    * **views** - OPTIONAL - holds the views
* css - holds the less/ scss / css or whatever css/preprocessor you will use
* fonts - if you want to use any


# Training task - PART 1

## User Interface:
* Create html and css
* Fluid layout
* Layout transformation on the following breakpoints [1025,768,320]
* Supported browsers - webkit based

## User Interface - Login Form components:

* Visual indicator showing you are on the login screen
* Labels for all inputs
* Input type text for the username
* Input type password
* Login button

## User Interface - Register Form components:

* Visual indicator showing you are on the register screen
* Labels for all inputs
* Input type text for the user name - min length 3 characters, not permitted special characters
* Input type password - min length 5 characters
* Input type password (repeat password) - min length 5 characters, checks if the 2 password fields have the same value
* Register button - do not let the user register if not all fields are correct

## User Interface - Logged In state components:

* Visual indicator showing you are logged in
* Show the user's name and a pre-defined user avatar
* Welcome message

## User Interface - Messages:
* design and implement messages to show the user useful info
* error messages - displayed if the user fails to login
* success messages - displayed for successful registration and login
* validation messages - display valid/invalid states of the user input

## Application modules and features

* Router (partial optional) - a basic script to change the application states (changing views and rendering new markup)
* View implementation (partial optional) - basic script to change html templates or render new templates
* Event binding - each time a view is display or rendered bind events in the templates for user interactions, validation and application action like: check if user is register, log in a user, showing error messages
* Event un binding - if a view is hidden, removed or destroyed please unbind all view events
* Data validation - validate data from user input like length of the entered user name for registration
* API communication - call API end points to: check, register, login a user
* Template engine (optional) - create a template engine that can render a HTML view and output JS data

## Additional notes

### API is documented in the WIKI section
### You must not use jQuery - http://youmightnotneedjquery.com/
### You may use any template engine you like - http://underscorejs.org/
### You may use any CSS/HTML framework - bootstrap, skeleton or whatever

## Resources:

### Template engine:

* http://krasimirtsonev.com/blog/article/Javascript-template-engine-in-just-20-line
* http://ejohn.org/blog/javascript-micro-templating/
* http://code.tutsplus.com/tutorials/best-practices-when-working-with-javascript-templates--net-28364
* http://www.bennadel.com/blog/2411-using-underscore-js-templates-to-render-html-partials.htm

### Useful resources:
* http://en.wikipedia.org/wiki/Single-page_application
* http://singlepageappbook.com/single-page.html
* https://github.com/visionmedia/page.js
* http://joakimbeng.eu01.aws.af.cm/a-javascript-router-in-20-lines/
* http://projects.jga.me/routie/

# Part 2 - coming soon